package inv.project.controller;

import inv.project.model.Persona;
import inv.project.service.PersonaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PersonaController {

	@Autowired
	private PersonaService personaService;

	//Agregar nueva persona
	@PostMapping("/persona")
	public ResponseEntity<?> save(@RequestBody Persona persona) {
		long id = personaService.save(persona);
		return ResponseEntity.ok()
				.body("New Book has been saved with ID:" + id);
	}

	//Obtener persona por id
	@GetMapping("/persona/{id}")
	public ResponseEntity<Persona> get(@PathVariable("id") int id) {
		Persona persona = personaService.get(id);
		return ResponseEntity.ok().body(persona);
	}
}
